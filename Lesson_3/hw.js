
// 1

let a = ['a', 'b', 'c'];

let b = [1, 2, 3];

console.log(a.concat(b));

// 2

let c = ['a', 'b', 'c'];
c.push(1, 2, 3);
console.log(c);

// 3

let num = [1, 2, 3];
console.log(num.reverse());

// 4

let num1 = [1, 2, 3];
num1.push(4, 5, 6)
console.log(num1);

// 5

let num2 = [1, 2, 3];
num2.unshift(4, 5, 6)
console.log(num2);

// 6

let d = ['js', 'css', 'jq'];
document.write(`<h1> ${d[0]} </h1>`);

// 7

let e = [1, 2, 3, 4, 5];

console.log(e.slice(0, 3));

// 8

let f = [1, 2, 3, 4, 5];
f.splice(1, 2);
console.log(f);

// 9

let g = [1, 2, 3, 4, 5];
g.splice(2, 0, 10);
console.log(g);

// 10

let num3 = [3, 4, 1, 2, 7];

console.log(num3.sort());

// 11

let helloWorld = ['Привіт, ', 'світ', '!'];
document.write(`<h1> ${helloWorld[0] + helloWorld[1] + helloWorld[2]}</h1>`)

//12

let helloWorld2 = ['Привіт, ', 'світ', '!'];
helloWorld2[0] = 'Бувай, ';
console.log(helloWorld2[0] + helloWorld2[1] + helloWorld2[2])

// 13

let arr = [1, 2, 3, 4, 5];
// or
let arr = new Array(1, 2, 3, 4, 5);

//14

var arr1 = [
    ['блакитний', 'червоний', 'зелений'],
    ['blue', 'red', 'green'],
];

console.log(arr1[0][0], arr1[1][0]);

// 15

let arr2 = ['a', 'b', 'c', 'd'];
console.log(`${arr2[0]} + ${arr2[1]}, ${arr2[2]} + ${arr2[3]}`);

// 16

let numOfArr = +prompt('Скільки елементів у масиві?');

    for(numOfArr = 0; numOfArr > 0 && numOfArr < 0; numOfArr++) {
        numOfArr = [];
    }