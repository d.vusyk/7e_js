// Створити клас Car , Engine та Driver.
// Клас Driver містить поля - ПІБ, стаж водіння.
// Клас Engine містить поля – потужність, виробник.
// Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

// Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
// Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.



class Driver {
    constructor(name, experience) {
      this.name = name;
      this.experience = experience;
    }
  }
  
  class Engine {
    constructor(power, manufacturer) {
      this.power = power;
      this.manufacturer = manufacturer;
    }
  }
  
  class Car {
    constructor(brand, carClass, weight, driver, engine) {
      this.brand = brand;
      this.carClass = carClass;
      this.weight = weight;
      this.driver = driver;
      this.engine = engine;
    }
  
    start() {
      console.log("Поїхали");
    }
  
    stop() {
      console.log("Зупиняємося");
    }
  
    turnRight() {
      console.log("Поворот праворуч");
    }
  
    turnLeft() {
      console.log("Поворот ліворуч");
    }
  
    toString() {
    return `Марка: ${this.brand}
    Клас: ${this.carClass}
    Вага: ${this.weight} кг
    Водій: ${this.driver.name}
    Стаж водіння: ${this.driver.experience} років
    Двигун: Потужність - ${this.engine.power} кінських сил, Виробник - ${this.engine.manufacturer}`;
    }
  }
  

  const driver = new Driver("Кирило Буданов", 5);
  const engine = new Engine(400, "VAG");
  const car = new Car("Audi", "Седан", 1500, driver, engine);
  

  console.log(car.toString());


  class Lorry extends Car {
    constructor(brand, carrying) {
        super(brand);
        this.carrying = carrying;
    }
    add() {
        console.log(`Вантажопідйомність ${this.brand} - ${this.carrying}`);
    }
  }

  class SportCar extends Car {
    constructor(brand, maxSpeed) {
        super(brand);
        this.maxSpeed = maxSpeed;
    }
    add() {
        console.log(`Максимальна швидкість ${this.brand} - ${this.maxSpeed}`);
    }
  }

const lorry = new Lorry('CAT', '1000 кг');
const sportCar = new SportCar('Pagani', '400 км/год');

lorry.add();
sportCar.add();