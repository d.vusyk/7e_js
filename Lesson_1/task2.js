// Task 2

let x = 6,
    y = 14,
    z = 4;


let result1 = x += y - x++ * z;

// // x += y = x = x + y = 20;
// // x++ * z = 6 * 4 = 24;
// // 20 - 24 = -4;

console.log(result1);



let result2 = z = --x - y * 5; // 5 - 70;

console.log(result2);



let result3 = y /= x + 5 % z;

// Згідно пріоритетів операторів спочатку беремо остачу від ділення:
// 5 % z = 1;
// Далі x + 1;
// Остання дія: 
// y /= 7 = 2;

console.log(result3);




let result4 = z - x + + + y * 5;

// - 2 + 70 = 68;

console.log(result4);




let result5 = x = y - x++ * z;

// Згідно пріоритетів операторів спочатку рахується постфіксний інкремент:
// x++ = 6;
// Далі:
// x++ * z = 24;
// x = -10;

console.log(result5);
