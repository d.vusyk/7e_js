// 4

// Даний рядок типу 'var_text_hello'. Зробіть із нього текст 'VarTextHello'.



function ucWords(str) {
    return console.log(str
    .split('_')
    .map((word) => word[0].toUpperCase() + word.slice(1))
    .join(''))
}

    ucWords('var_text_hello');



//5

// Функція ggg приймає 2 параметри: анонімну функцію, яка повертає 3 та анонімну функцію, яка
// повертає 4. Поверніть результатом функції ggg суму 3 та 4.


function ggg (param1, param2) {
    return param1 + param2
}

console.log(ggg(3, 4))
   


//6

// Створіть об'єкт криптокошилок. У гаманці має зберігатись ім'я власника, кілька валют Bitcoin, Ethereum Stellar і в кожній валюті додатково є ім'я валюти, логотип, кілька монет та курс на сьогоднішній день
//Також в об'єкті гаманець є метод при виклику якого він приймає ім'я валюти та виводить на сторінку  інформацію."Доброго дня, ... ! На вашому балансі (Назва валюти та логотип) залишилося N монет, якщо ви сьогодні продасте їх те, отримаєте ...грн.


let walletCrypto = {
    name: 'Дмитро',
    bitcoin: {
        name: 'Bitcoin',
        logo: 'logo',
        quantity: '2',
        exchangeRateToday: '1076384.73',
    },
    eth: {
        name: 'Ethereum Stellar',
        logo: 'Ethereum Stellar logo',
        quantity: '5',
        exchangeRateToday: '69 822.37',
    }
}

    walletCrypto.currencyName = function () {
       return `Доброго дня, ${walletCrypto.name} ! На вашому балансі ${walletCrypto.bitcoin.name} ${walletCrypto.bitcoin.logo} залишилося ${walletCrypto.bitcoin.quantity} монет, якщо ви сьогодні продасте їх те, отримаєте ${walletCrypto.bitcoin.quantity * walletCrypto.bitcoin.exchangeRateToday} грн.`
    }
    
    console.log(walletCrypto.currencyName());


    //7

    // Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
    // rate (ставка за день роботи), days (кількість відпрацьованих днів).
    // Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
    // Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.

    class Worker {
        constructor () {
            this.name = 'Taras';
            this.surname = 'Tarasovich';
            this.rate = 100;
            this.days = 22;
        }
        getSalary() {
            return console.log(`Заробітна плата ${this.name} за ${this.days} робочих дня становить ${this.rate * this.days}$`);
        }
    }

    new Worker().getSalary();


    // Реалізуйте клас MyString, який матиме такі методи:
    // метод reverse(), який параметром приймає рядок, а повертає її в перевернутому вигляді,
    // метод ucFirst(), який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою та 
    // метод ucWords(), який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.

    class MyString {
        reverse(str) {
            return console.log(str.split('').reverse().join(''));   
        }

        ucFirst(str1) {
            return console.log(str1.split(' '))    // не розумію, як далі робити
        }

        ucWords(str2) {
            return console.log(str2.split(' ').map((word) => word[0].toUpperCase() + word.slice(1)).join(' '))
        }
    }

    new MyString().reverse('reverse');
    new MyString().ucFirst('Reverse Reverse Reverse Reverse');
    new MyString().ucWords('reverse reverse reverse reverse');
