
// Створити конструктор Animal та розширюючі його конструктори Dog, Cat, Horse.
// Конструктор Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
// Dog, Cat, Horse перевизначають методи makeNoise, eat.
// Додайте змінні до конструкторів Dog, Cat, Horse, що характеризують лише цих тварин.
// Створіть конструктор Ветеринар, у якому визначте метод який виводить в консоль treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
// У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.

function Animal(food, location) {
    this.food = 'Корм';
    this.location = 'Європа';
} 

Animal.prototype.makeNoise = function() {
    console.log(`${this.name} спить`);
};
Animal.prototype.eat = function() {
    console.log(`${this.name} їсть`);
};
Animal.prototype.sleep = function() {
    console.log(`Тварина спить`);
};

function Dog(name, food, location) {
    this.name = name;
    this.food = food;
    this.location = location;
    // this.age = age; 
}

Dog.prototype = new Animal();

Dog.prototype.makeNoise = function () {
    console.log(`${this.name} спить 10 годин`);
}

Dog.prototype.eat = function () {
    console.log(`${this.name} їсть корм`);
}


function Cat(name, age) {
    this.name = name;
    // this.age = age; 
}
Cat.prototype = new Animal();

function Horse(name, age) {
    this.name = name;
    // this.age = age; 
}

Horse.prototype = new Animal();


function Doc () {

}

Doc.prototype.treatAnimal = function (animal) {
    console.log(Object.values(animal))
}

function main () {
    const animal = [new Dog(), new Cat(), new Horse()];

    animal.forEach((item)=> {
        new Doc().treatAnimal(item)
    })
}



// Важко мені дається це все наслідування, прототипи і тд... Далі не знаю, як робити. Напишіть будь ласка розв'язок